# Custom OLSRd2 for PredicTAKE

This repository contains a custom version of OLSRd2 used for the PredicTAKE project. This fork of OLSRd2 transports S2D (Source to Destination) and GPS positions inside custom TLVs of OLSR Hello Messages.

The following files have been modified:

- `src-plugins/nhdp/nhdp/nhdp_reader.c`
- `src-plugins/nhdp/nhdp/nhdp_writer.c`
- `src-plugins/subsystems/rfc5444/rfc5444_iana.h`



## Installation

Get the code from the PredicTAKE Gitlab (https://gitlab.forge.hefr.ch/predictake)

```shell
git clone git@gitlab.forge.hefr.ch:predictake/OONF.git
```

#### Pre-requisites

See *[[switchdrive](https://drive.switch.ch)]/Armasuisse/PredicTAKE/OLSRv2/1 - OLSRv2 Installation.docx* for the original OLSRd2 requirements.

#### Compiling

Follow these commands to update, build and install the customized OLSRd2:

```shell
cd ~/OONF
rm *.deb
git pull
cd build/
cmake ..
make
cd ..
cd files
./create_debian_package.sh
cd ..
sudo dpkg -i olsrd2_0.15.x-xx_amd64.deb
sudo update-rc.d olsrd2 disable
sudo reboot
```
#### Auto install

To automatically update, compile and install the customized OLSRd2, you can simply run the following shell script:

```shell
cd ~/OONF/install
./updateBuildInstallOONF.sh
```



## References

- See *README_original.md* for the original OLSRd2 README from OONF.
- See *[[switchdrive](https://drive.switch.ch)]/Armasuisse/PredicTAKE/Machine Learning/S2D/S2D_documentation.docx* for the S2D documentation.



Gilles Waeber
2018-07-06