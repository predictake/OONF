set(TESTS test_rfc5444_reader_blockcb
          test_rfc5444_reader_dropcontext
          test_rfc5444_writer_fragmentation
          test_rfc5444_writer_ifspecific
          test_rfc5444_writer_mandatory
          test_rfc5444
          )
set (LIBS oonf_common oonf_config)

include_directories(${CMAKE_SOURCE_DIR}/src-plugins/subsystems)
foreach(TEST ${TESTS})
    oonf_create_test(${TEST} "${TEST}.c" "${LIBS}" "$<TARGET_OBJECTS:oonf_static_rfc5444_api>")
endforeach(TEST)

add_subdirectory(interop2010)
add_subdirectory(special)
