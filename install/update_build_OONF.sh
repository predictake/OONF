#!/bin/sh

cd ~/OONF
rm *.deb

git pull

cd build/
cmake ..
make
cd ..

cd files
chmod +x create_debian_package.sh
./create_debian_package.sh
cd ..
